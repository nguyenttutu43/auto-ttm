import { Locator, Page, expect } from "@playwright/test"


export class PaymentPage {
    page: Page;
    paymentTab: Locator;
    paymentTitle: Locator;
    ecomdyBalance: Locator;
    visaMethod: Locator;
    paypalMethod: Locator;
    payoneerMethod: Locator;
    wiseMethod: Locator;
    usdtMethod: Locator;
    airwallexMethod: Locator
    tazapayMethod: Locator;
    lianlianMethod: Locator;
    creditCardTad: Locator;
    creditCardTitle: Locator;
    creditCardAddFunBlock: Locator
    inputAmount: Locator;
    button200: Locator;
    button500: Locator;
    button1000: Locator;
    button2000: Locator;
    button5000: Locator;
    validMessage: Locator;
    topupButton:Locator;
    serviceCharge: Locator;
    creditCardFee: Locator;
    tax: Locator;
    amountInSummary: Locator;
    paymentMethod: Locator;
    serviceChangeInSummary: Locator;
    creditCardFeeInSummary: Locator;
    taxInSummary: Locator;
    totalAmount: Locator;


    constructor ( page: Page ) {
        this.page = page;
        this.paymentTab = page.getByTestId('tab-layout.textpayment-navbar');
        this.paymentTitle = page.locator('.content-header-title.float-left.pr-1.mb-0.border-right-0');
        this.ecomdyBalance = page.locator('.money.font-medium');
        this.visaMethod = page.locator('#trigger-credit-card');
        this.paypalMethod = page.locator('#trigger-paypal');
        this.payoneerMethod = page.locator('#trigger-payoneer');
        this.wiseMethod = page.locator('#trigger-wise');
        this.usdtMethod = page.locator('#trigger-usdt');
        this.airwallexMethod = page.locator('#trigger-airwallex');
        this.tazapayMethod = page.locator('#trigger-tazapay');
        this.lianlianMethod = page.locator('#trigger-lianlian');
        this.creditCardTad = page.locator('#trigger-credit-card-add-fund___BV_tab_button__');
        this.creditCardTitle = page.locator('h4', { hasText: 'Credit Card' });
        this.creditCardAddFunBlock = page.locator('.list-card.bg-white.credit-card-top-up-form.position-relative')
        this.button200 = page.locator('#trigger-credit-card-add-fund').getByTestId('amount-200');
        this.button500 = page.locator('#trigger-credit-card-add-fund').getByTestId('amount-500');
        this.button1000 = page.locator('#trigger-credit-card-add-fund').getByTestId('amount-1000');
        this.button2000 = page.locator('#trigger-credit-card-add-fund').getByTestId('amount-2000');
        this.button5000 = page.locator('#trigger-credit-card-add-fund').getByTestId('amount-5000');
        this.inputAmount = page.locator('#trigger-credit-card-add-fund').getByTestId('input-amount-add-fund');
        this.validMessage = page.locator('#trigger-credit-card-add-fund').locator('#amount').locator('//small');
        this.topupButton = page.getByTestId('trigger-top-up-cc');
        this.serviceCharge = page.getByTestId('percent-service-charge-credit-card-fee');
        this.creditCardFee = page.getByTestId('percent-credit-card-fee');
        this.tax = page.getByTestId('percent-tax-credit-card-fee');
        this.amountInSummary = page.locator('.summary-container').getByTestId('amount-credit-card');
        this.paymentMethod = page.getByTestId('credit-card-method');
        this.serviceChangeInSummary = page.getByTestId('service-charge-credit-card');
        this.creditCardFeeInSummary = page.locator('.summary-container').getByTestId('credit-card-fee');
        this.taxInSummary = page.getByTestId('tax-fee-credit-card');
        this.totalAmount = page.locator('.summary-container').getByTestId('total-amount-credit-card');

    }

    async clickPaymentTab() {
        await this.paymentTab.click();
    }

    async openPaymentPageSuccess() {
        await expect(this.paymentTitle).toHaveText('Payment');
        await expect(this.ecomdyBalance).toBeVisible();
    }

    async showAllPaymentMethod() {
        await expect(this.visaMethod).toBeVisible();
        await expect(this.paypalMethod).toContainText(' PayPal ');
        await expect(this.payoneerMethod).toContainText(' payoneer ');
        await expect(this.wiseMethod).toContainText(' WISE ');
        await expect(this.usdtMethod).toContainText(' USDT ');
        await expect(this.airwallexMethod).toContainText(' Airwallex ');
        await expect(this.tazapayMethod).toContainText(' tazapay ');
        await expect(this.lianlianMethod).toContainText(' LianLian ');
    }

    async clickCreditCardMethod() {
        await this.visaMethod.click();
    }

    async openCreditCardPageSuccess() {
        await expect(this.creditCardTad).toHaveClass('nav-link active')
        await expect(this.creditCardTitle).toBeVisible();

    }

    async clickButton200() {
        await this.button200.click();
        await this.button200.click();
    }
    
    async clickButton500() {
        await this.button500.click();
        await this.button500.click();
    }

    async clickButton1000() {
        await this.button1000.click();
        await this.button1000.click();
    }

    async clickButton2000() {
        await this.button2000.click();
        await this.button2000.click();
    }

    async clickButton5000() {
        await this.button5000.click();
        await this.button5000.click();
    }

    async message() {
        const message = await this.validMessage.textContent();
        expect(message).toBe(' This amount is suitable ');
    }

    async showInputAmountCorrect({ amount }) {
        await expect(this.inputAmount).toHaveValue(amount);
    }
    
    async enableTopUpButton() {
        await expect(this.topupButton.first()).not.toBeDisabled();
    }

    async inputvalidAmount({ amount }) {
        await this.inputAmount.fill(amount);
    }

    async feeOnSummaryCorrect() {
        await expect(this.serviceCharge).toHaveText('(3.00%)');
        await expect(this.creditCardFee).toHaveText('(4.00%)');
        await expect(this.tax).toHaveText('(3.00%)')
    }

    async showSummaryCorrect({ amount }) {
        const inputAmount = +amount.toString() + " " + "USD"
        await expect(this.amountInSummary).toHaveText(inputAmount);
        await expect(this.paymentMethod).toHaveText('Visa');
        const serviceCharge = (+amount * 0.03).toString() + " " + "USD";
        await expect(this.serviceChangeInSummary).toHaveText(serviceCharge);
        const creditCardFee = (+amount * 0.04).toString() + " " + "USD";
        await expect(this.creditCardFeeInSummary).toHaveText(creditCardFee);
        const tax = (+amount * 0.03).toString() + " " + "USD";
        await expect(this.taxInSummary).toHaveText(tax);
        const total = (+amount + +amount * 0.03 + +amount * 0.04 + +amount * 0.03).toString() + " " + "USD";
        await expect(this.totalAmount).toHaveText(total);

    }



}