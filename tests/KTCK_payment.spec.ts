import { test } from '@playwright/test';
import { DemoLoginPage } from '../src/page/login_page'
import { PaymentPage } from '../src/page/payment_page'


/*
TC1: Verify Payment page shows when clicking on Payment link in menu bar.
TC2: Verify all payment methods shows when opening Payment page
TC3: Verify Credit card page shows when clicking on Credit card in Payment page
TC4: Verify input amount shows correctly when clicking on 200, 500, 1000, 2000, 5000 button
TC5: Verify all fee is calculated correctly when enter valid data into amount input

*/

test.beforeEach ('login', async ({ page }) => {
    const demoLogin = new DemoLoginPage(page);
    await demoLogin.goto();
    const email = 'nguyentu007979+1@gmail.com'
    const password = '12341234'
    await demoLogin.enterData({ email, password });
    await demoLogin.clickOnLoginButton();
    await demoLogin.verifylogin();
})

test.describe('test payment credit card',async () => {
    test('TC1: Verify Payment page shows when clicking on Payment link in menu bar', async ({ page }) => {
        const payment = new PaymentPage(page);
        await payment.clickPaymentTab();
        await payment.openPaymentPageSuccess();
    })

    test('TC2: Verify all payment methods shows when opening Payment page',async ({ page }) => {
        const payment = new PaymentPage(page);
        await payment.clickPaymentTab();
        await payment.showAllPaymentMethod();
        
     })

    test('TC3: Verify Credit card page shows when clicking on Credit card in Payment page', async ({ page }) => {
        const payment = new PaymentPage(page);
        await payment.clickPaymentTab();
        await payment.clickCreditCardMethod();
        await payment.openCreditCardPageSuccess();
        
    })

    test('TC4.1: Verify input amount shows correctly when clicking on 200 button', async ({ page }) => {
        const payment = new PaymentPage(page);
        await payment.clickPaymentTab();
        await payment.clickCreditCardMethod();
        await payment.clickButton200();
        const amount = '200';
        await payment.showInputAmountCorrect({ amount });
        await payment.message();
        await payment.enableTopUpButton();
        
    })

    test('TC4.2: Verify input amount shows correctly when clicking on 500 button',async ({ page }) => {
        const payment = new PaymentPage(page);
        await payment.clickPaymentTab();
        await payment.clickCreditCardMethod();
        await payment.clickButton500();
        const amount = '500';
        await payment.showInputAmountCorrect({ amount });
        await payment.message();
        await payment.enableTopUpButton();
    })

    test('TC4.3: Verify input amount shows correctly when clicking on 1000 button',async ({ page }) => {
        const payment = new PaymentPage(page);
        await payment.clickPaymentTab();
        await payment.clickCreditCardMethod();
        await payment.clickButton1000();
        const amount = '1000';
        await payment.showInputAmountCorrect({ amount });
        await payment.message();
        await payment.enableTopUpButton();
        
    })

    test('TC4.4: Verify input amount shows correctly when clicking on 2000 button',async ({ page }) => {
        const payment = new PaymentPage(page);
        await payment.clickPaymentTab();
        await payment.clickCreditCardMethod();
        await payment.clickButton2000();
        const amount = '2000';
        await payment.showInputAmountCorrect({ amount });
        await payment.message();
        await payment.enableTopUpButton();

    })

    test('TC4.5: Verify input amount shows correctly when clicking on 5000 button',async ({ page }) => {
        const payment = new PaymentPage(page);
        await payment.clickPaymentTab();
        await payment.clickCreditCardMethod();
        await payment.clickButton5000();
        const amount = '5000';
        await payment.showInputAmountCorrect({ amount });
        await payment.message();
        await payment.enableTopUpButton();
    
    })

    test.only('TC5: Verify all fee is calculated correctly when enter valid data into amount input',async ({ page }) => {
        const payment = new PaymentPage(page);
        await payment.clickPaymentTab();
        await payment.clickCreditCardMethod();
        const amount = '200'
        await payment.inputvalidAmount({ amount });
        await payment.message();
        await payment.feeOnSummaryCorrect();
        await payment.showSummaryCorrect({ amount });
    })

    
})